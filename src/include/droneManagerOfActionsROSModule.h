//////////////////////////////////////////////////////
//  droneManagerOfActionsROSModule.h
//
//  Created on: July 29, 2015
//      Author: carlos sampedro
//
//  Last modification on: July 29, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////


#ifndef _DRONE_ARUCO_EYE_ROS_MODULE_H
#define _DRONE_ARUCO_EYE_ROS_MODULE_H

#include <iostream>
#include <string>
#include <vector>

// ROS
#include "ros/ros.h"
#include "droneModuleROS.h"
#include "droneManager.h"
#include "communication_definition.h"

// Flying modes
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneCommand.h"



#include "std_msgs/String.h"


/////////////////////////////////////////
// Class DroneManagerOfActionsROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneManagerOfActionsROSModule : public DroneModule
{
private:
    DroneManager MyDroneDroneManager;
    droneMsgsROS::droneStatus last_drone_status_msg;

public: // Constructors and destructors
    DroneManagerOfActionsROSModule();
    ~DroneManagerOfActionsROSModule();

    void open(ros::NodeHandle & nIn);
    void droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);
    void missionToManagerCallback(const std_msgs::String::ConstPtr& msg);
    ros::Publisher DroneCommandPubl;
    ros::Subscriber drone_status_subscriber;
    ros::Subscriber drone_action_suscriber;



};




#endif

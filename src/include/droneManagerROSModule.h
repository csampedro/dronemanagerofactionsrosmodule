//////////////////////////////////////////////////////
//  droneManagerROSModule.h
//
//  Created on: July 29, 2015
//      Author: carlos sampedro
//
//  Last modification on: July 29, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////


#ifndef _DRONEMANAGER_ROS_MODULE_H
#define _DRONEMANAGER_ROS_MODULE_H

#include <iostream>
#include <string>
#include <vector>

// ROS
#include "ros/ros.h"
#include "droneModuleROS.h"
#include "droneManager.h"
#include "communication_definition.h"

// Flying modes
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneCommand.h"



#include "std_msgs/String.h"


/////////////////////////////////////////
// Class DroneManagerROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneManagerROSModule : public DroneModule
{
private:
    DroneManager MyDroneManager;

public: // Constructors and destructors
    DroneManagerROSModule();
    ~DroneManagerROSModule();

    void open(ros::NodeHandle & nIn);

protected:
    std::string configFile;

public:
    void readParameters();



};




#endif

//I/O Stream
//std::cout
#include <iostream>
#include <string>
#include <sstream>

// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "dronemoduleinterface.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "control/simpletrajectorywaypoint.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"

#include <thread>

using namespace std;


//Define step commands
#define CTE_COMMAND_YAW    0.40
#define CTE_COMMAND_PITCH  0.33
#define CTE_COMMAND_ROLL   0.33
#define CTE_COMMAND_HEIGHT 0.50


// Define controller commands define constants
#define CONTROLLER_CTE_COMMAND_SPEED        ( 1.00 )
#define CONTROLLER_STEP_COMMAND_POSITTION   ( 0.25 )
#define CONTROLLER_STEP_COMMAND_ALTITUDE    ( 0.25 )
#define CONTROLLER_STEP_COMMAND_YAW         ( 10.0 * (M_PI/180.0) )


droneMsgsROS::dronePose   current_drone_position_reference;
droneMsgsROS::dronePose   last_drone_estimated_GMRwrtGFF_pose;
void managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg)
{
    //ROS_INFO("I heard: [%s]", msg->time);
    cout<<"Mission Planner: Received"<<endl;
}

void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    current_drone_position_reference = (*msg);
}

void drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose  = (msg);
}

void thread_mission_planner_command_fnc()
{
    ros::NodeHandle n;

    ros::Publisher mission_planer_pub = n.advertise<droneMsgsROS::droneMissionPlannerCommand>("droneMissionPlannerCommand", 1, true);
    ros::Publisher drone_position_reference_publisher =
            n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    ros::Publisher DroneDAltitudeCmdPubl = n.advertise<droneMsgsROS::droneDAltitudeCmd>("command/dAltitude",1);
    ros::Publisher DroneDYawCmdPubl=n.advertise<droneMsgsROS::droneDYawCmd>("command/dYaw",1, true);

    ros::Subscriber droneHLCommAckSub = n.subscribe("droneMissionHLCommandAck", 1, managerAckCallback);
    ros::Subscriber drone_pos_reference_subs =
            n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, droneCurrentPositionRefsSubCallback);

    ros::Subscriber drone_estimated_pose_subs = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, drone_estimated_GMR_pose_callback);



    //ros::Rate loop_rate(10);


    droneMsgsROS::droneDAltitudeCmd DroneDAltitudeCmdMsgs;
    droneMsgsROS::droneDYawCmd DroneDYawCmdMsgs;
    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;



    char tecla='i';
    while (ros::ok())
    {
        droneMsgsROS::droneMissionPlannerCommand msg;
        int32_t mp_command;
        cin.clear();
        cin>>tecla;




        if(tecla==27)
            break;
        else if(tecla=='t')
        {
            cout<<"Command takeoff() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
        }
        else if(tecla=='l')
        {
            cout<<"Command land() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::LAND;
        }
        else if(tecla=='h')
        {
            cout<<"Command hover() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::HOVER;
        }
        else if(tecla=='q')
        {
            cout<<"Command move() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD;

            DroneDAltitudeCmdMsgs.dAltitudeCmd = CTE_COMMAND_HEIGHT;
            DroneDAltitudeCmdPubl.publish(DroneDAltitudeCmdMsgs);

        }

        else if(tecla=='a')
        {
            cout<<"Command move() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD;

            DroneDAltitudeCmdMsgs.dAltitudeCmd = -CTE_COMMAND_HEIGHT;
            DroneDAltitudeCmdPubl.publish(DroneDAltitudeCmdMsgs);
        }

        else if(tecla == 'z')
        {
            cout<<"Command move() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD;

            DroneDYawCmdMsgs.dYawCmd = -CTE_COMMAND_YAW;
            DroneDYawCmdPubl.publish(DroneDYawCmdMsgs);
        }

        else if(tecla == 'x')
        {
            cout<<"Command move() sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD;

            DroneDYawCmdMsgs.dYawCmd = CTE_COMMAND_YAW;
            DroneDYawCmdPubl.publish(DroneDYawCmdMsgs);
        }

        else if(tecla == 'r' || tecla == 'R')
        {
            cout<<"comand move() IN POSITION sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
            double current_xs, current_ys, current_zs, current_yaws;
            current_xs = last_drone_estimated_GMRwrtGFF_pose.x;
            current_ys = last_drone_estimated_GMRwrtGFF_pose.y;
            current_zs = last_drone_estimated_GMRwrtGFF_pose.z;
            current_yaws = last_drone_estimated_GMRwrtGFF_pose.yaw;


            drone_position_reference.header.stamp  = ros::Time::now();
            drone_position_reference.position_command.x = current_xs + CONTROLLER_STEP_COMMAND_POSITTION;
            drone_position_reference.position_command.y = current_ys;
            drone_position_reference.position_command.z = current_zs;


            drone_position_reference_publisher.publish(drone_position_reference);
        }

        else if(tecla == 'f' || tecla == 'F')
        {
            cout<<"comand move() IN POSITION sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
            double current_xs, current_ys, current_zs, current_yaws;
            current_xs = last_drone_estimated_GMRwrtGFF_pose.x;
            current_ys = last_drone_estimated_GMRwrtGFF_pose.y;
            current_zs = last_drone_estimated_GMRwrtGFF_pose.z;
            current_yaws = last_drone_estimated_GMRwrtGFF_pose.yaw;


            drone_position_reference.header.stamp  = ros::Time::now();
            drone_position_reference.position_command.x = current_xs - CONTROLLER_STEP_COMMAND_POSITTION;
            drone_position_reference.position_command.y = current_ys;
            drone_position_reference.position_command.z = current_zs;


            drone_position_reference_publisher.publish(drone_position_reference);
        }

        else if(tecla == 'd' || tecla == 'D')
        {
            cout<<"comand move() IN POSITION sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
            double current_xs, current_ys, current_zs, current_yaws;
            current_xs = last_drone_estimated_GMRwrtGFF_pose.x;
            current_ys = last_drone_estimated_GMRwrtGFF_pose.y;
            current_zs = last_drone_estimated_GMRwrtGFF_pose.z;
            current_yaws = last_drone_estimated_GMRwrtGFF_pose.yaw;


            drone_position_reference.header.stamp  = ros::Time::now();
            drone_position_reference.position_command.x = current_xs;
            drone_position_reference.position_command.y = current_ys - CONTROLLER_STEP_COMMAND_POSITTION;
            drone_position_reference.position_command.z = current_zs;


            drone_position_reference_publisher.publish(drone_position_reference);
        }

        else if(tecla == 'g' || tecla == 'G')
        {
            cout<<"comand move() IN POSITION sent"<<endl;
            mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
            double current_xs, current_ys, current_zs, current_yaws;
            current_xs = last_drone_estimated_GMRwrtGFF_pose.x;
            current_ys = last_drone_estimated_GMRwrtGFF_pose.y;
            current_zs = last_drone_estimated_GMRwrtGFF_pose.z;
            current_yaws = last_drone_estimated_GMRwrtGFF_pose.yaw;


            drone_position_reference.header.stamp  = ros::Time::now();
            drone_position_reference.position_command.x = current_xs;
            drone_position_reference.position_command.y = current_ys + CONTROLLER_STEP_COMMAND_POSITTION;
            drone_position_reference.position_command.z = current_zs;


            drone_position_reference_publisher.publish(drone_position_reference);
        }


        msg.mpCommand = mp_command;
        mission_planer_pub.publish(msg);


        ros::spinOnce();

        //loop_rate.sleep();
    }

    return;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "missionPlannerEmulator");
    ros::NodeHandle n;


    std::thread thread_mission_planner_command(thread_mission_planner_command_fnc);



    ros::spin();



    return 0;
}

void sendCommandInPositionControlMode()
{

}








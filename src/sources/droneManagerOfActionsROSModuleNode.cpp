#include <iostream>
#include <string>
#include <vector>


#include "ros/ros.h"

#include "droneManagerOfActionsROSModule.h"

using namespace std;



int main(int argc, char **argv)
{
    ros::init(argc, argv, "managerListener");
    ros::NodeHandle n;


    DroneManagerOfActionsROSModule MyDroneManagerOfActionsROSModule;
    MyDroneManagerOfActionsROSModule.open(n);

    while(ros::ok())
    {
        ros::spin();
    }
    return 0;
}






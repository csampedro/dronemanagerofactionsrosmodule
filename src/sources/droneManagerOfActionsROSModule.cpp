//////////////////////////////////////////////////////
//  droneManagerOfActionsROSModule.cpp
//
//  Created on: July 29, 2015
//      Author: carlos sampedro
//
//  Last modification on: July 29, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////


#include "droneManagerOfActionsROSModule.h"
using namespace std;

DroneManagerOfActionsROSModule::DroneManagerOfActionsROSModule(): DroneModule(droneModule::active)
{

}


DroneManagerOfActionsROSModule::~DroneManagerOfActionsROSModule()
{

}

void DroneManagerOfActionsROSModule::droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg) {
    // See comment on ardrone_autonomy/msg/Navdata.msg
    // # 0: Unknown, 1: Init, 2: Landed, 3: Flying, 4: Hovering, 5: Test
    // # 6: Taking off, 7: Goto Fix Point, 8: Landing, 9: Looping
    // # Note: 3,7 seems to discriminate type of flying (isFly = 3 | 7)
    last_drone_status_msg = (*msg);

    if(last_drone_status_msg.status==droneMsgsROS::droneStatus::HOVERING)
    {
        cout<<"MidLevel in HOVERING"<<endl;
        MyDroneDroneManager.hover();
    }
    else if(last_drone_status_msg.status==droneMsgsROS::droneStatus::LANDING)
    {
        cout<<"MidLevel in LANDING"<<endl;
        MyDroneDroneManager.land();
    }
    else if(last_drone_status_msg.status==droneMsgsROS::droneStatus::TAKING_OFF)
    {
        cout<<"MidLevel in TAKINOFF"<<endl;
        MyDroneDroneManager.takeoff();

    }

}

void DroneManagerOfActionsROSModule::missionToManagerCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("I heard: [%s]", msg->data.c_str());

    if(msg->data=="hover")
    {
        cout<<"HOVER to the state machine"<<endl;
        //MyDroneDroneManager.hover();
        droneMsgsROS::droneCommand DroneCommandMsgs;
        DroneCommandMsgs.command = DroneCommandMsgs.HOVER;
        DroneCommandPubl.publish(DroneCommandMsgs);
    }
    else if(msg->data=="land")
    {
        cout<<"LAND to the state machine"<<endl;
        //MyDroneDroneManager.land();
        droneMsgsROS::droneCommand DroneCommandMsgs;
        DroneCommandMsgs.command = DroneCommandMsgs.LAND;
        DroneCommandPubl.publish(DroneCommandMsgs);
    }
    else if(msg->data=="takeoff")
    {
        cout<<"TAKEOFF to the state machine"<<endl;
        //MyDroneDroneManager.takeoff();
        droneMsgsROS::droneCommand DroneCommandMsgs;
        DroneCommandMsgs.command = DroneCommandMsgs.TAKE_OFF;
        DroneCommandPubl.publish(DroneCommandMsgs);
    }

}


void DroneManagerOfActionsROSModule::open(ros::NodeHandle & nIn)
{
    DroneModule::open(nIn);

    drone_status_subscriber=n.subscribe("drone1/status", 1, &DroneManagerOfActionsROSModule::droneStatusCallback, this);

    DroneCommandPubl=n.advertise<droneMsgsROS::droneCommand>("drone1/command/high_level",1, true);

    drone_action_suscriber = n.subscribe("missionPlanner_to_manager", 1000, &DroneManagerOfActionsROSModule::missionToManagerCallback, this);

}



//////////////////////////////////////////////////////
//  droneManagerROSModule.cpp
//
//  Created on: July 29, 2015
//      Author: carlos sampedro
//
//  Last modification on: July 29, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////


#include "droneManagerROSModule.h"
using namespace std;

DroneManagerROSModule::DroneManagerROSModule(): DroneModule(droneModule::active)
{

}


DroneManagerROSModule::~DroneManagerROSModule()
{

}

void DroneManagerROSModule::readParameters()
{
    // Parameters
    // Config file
    ros::param::get("~config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="drone_module_drivers.xml";
    }


    return;
}

void DroneManagerROSModule::open(ros::NodeHandle & nIn)
{
    DroneModule::open(nIn);

    readParameters();
    MyDroneManager.open(n);
    MyDroneManager.init(stackPath+"configs/drone"+stringId+"/"+configFile);

    moduleStarted=true;
}



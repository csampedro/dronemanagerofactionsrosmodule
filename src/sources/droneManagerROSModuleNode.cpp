#include <iostream>
#include <string>
#include <vector>


#include "ros/ros.h"

#include "droneManagerROSModule.h"

using namespace std;



int main(int argc, char **argv)
{
    ros::init(argc, argv, "Manager");
    ros::NodeHandle n;


    DroneManagerROSModule MyDroneManagerROSModule;
    MyDroneManagerROSModule.open(n);

    while(ros::ok())
    {
        ros::spin();
    }
    return 0;
}





